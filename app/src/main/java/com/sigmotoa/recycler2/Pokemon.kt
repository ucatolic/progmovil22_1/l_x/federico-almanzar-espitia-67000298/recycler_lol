package com.sigmotoa.recycler2

import android.media.Image
import androidx.annotation.DrawableRes

data class Pokemon(
    val id: Long,
    val name: String,
    val type: String,
    val Q: String,
    val W: String,
    val E: String,
    val R: String,
    @DrawableRes
    val image: Int?
    )
