package com.sigmotoa.recycler2

import android.content.res.Resources
import com.sigmotoa.recycler2.R

class Pokemons {
    companion object{
        val pokemonList = listOf<Pokemon>(
            Pokemon(
                id=1,
                name="Aatrox",
                type="Coloso",
                Q="LA ESPADA DE LOS OSCUROS",
                W="CADENAS INFERNALES",
                E="DESLIZAMIENTO SOMBRÍO",
                R="EL ANIQUILADOR DE MUNDOS",
                image = R.drawable.aatrox
            ),
            Pokemon(
                id=2,
                name="Ahri",
                type="Mago",
                Q="ORBE DEL ENGAÑO",
                W="FUEGO ZORRUNO",
                E="HECHIZAR",
                R="IMPULSO ESPIRITUAL",
                image = R.drawable.ahri
            ),
            Pokemon(
                id=3,
                name="Akali",
                type="Asesina",
                Q="PLENO DE CINCO PUNTAS",
                W="VELO DEL CREPÚSCULO",
                E="VOLTERETA SHURIKEN",
                R="EJECUCIÓN PERFECTA",
                image = R.drawable.akali
            ),
            Pokemon(
                id=4,
                name="Akshan",
                type="Tirador",
                Q="BUMERÁN VENGADOR",
                W="REBELDE",
                E="BALANCEO HEROICO",
                R="MERECIDO",
                image = R.drawable.akshan
            ),
            Pokemon(
                id=5,
                name="Alistar",
                type="Tanque",
                Q="PULVERIZACIÓN",
                W="TESTARAZO",
                E="PISOTEAR",
                R="VOLUNTAD INQUEBRANTABLE",
                image = R.drawable.alistar
            ),
            Pokemon(
                id=6,
                name="Amumu",
                type="Tanque",
                Q="LANZAMIENTO DE VENDAS",
                W="DESESPERACIÓN",
                E="BERRINCHE",
                R="MALDICIÓN DE LA MOMIA TRISTE",
                image = R.drawable.amumu
            ),
            Pokemon(
                id=7,
                name="Anivia",
                type="Mago",
                Q="DESTELLO HELADO",
                W="CRISTALIZACIÓN",
                E="CONGELACIÓN",
                R="TORMENTA GLACIAL",
                image = R.drawable.anivia
            ),
            Pokemon(
                id=8,
                name="DR Mundo",
                type="Coloso",
                Q="SERRUCHO INFECTADO",
                W="DESISFIBRULACIÓN",
                E="TRAUMATISMO",
                R="DOSIS MÁXIMA",
                image = R.drawable.drmundo
            ),
            Pokemon(
                id=9,
                name="Irelia",
                type="Coloso",
                Q="EMBATE DE ESPADA",
                W="DANZA DESAFIANTE",
                E="DÚO IMPECABLE",
                R="FILO DE LA VANGUARDIA",
                image = R.drawable.irelia
            ),
            Pokemon(
                id=10,
                name="Jhin",
                type="Tirador",
                Q="GRANADA DANZANTE",
                W="FLORECER MORTAL",
                E="PÚBLICO ENTREGADO",
                R="ABAJO EL TELÓN",
                image = R.drawable.jhin
            ),
            Pokemon(
                id=11,
                name="Jinx",
                type="Tirador",
                Q="¡CAMBIAZO!",
                W="¡ZAP!",
                E="¡MASCAFUEGOS!",
                R="¡SUPERMEGACOHETE MORTAL!",
                image = R.drawable.jinx
            ),
            Pokemon(
                id=12,
                name="Kai'sa",
                type="Tirador",
                Q="ORBE DEL ENGAÑO",
                W="FUEGO ZORRUNO",
                E="HECHIZAR",
                R="IMPULSO ESPIRITUAL",
                image = R.drawable.kaisa
            ),
            Pokemon(
                id=13,
                name="Xayah",
                type="Tirador",
                Q="DAGA DOBLE",
                W="PLUMAJE MORTÍFERO",
                E="DAGAS A MÍ",
                R="TORMENTA DE PLUMAS",
                image = R.drawable.xayah
            ),
            Pokemon(
                id=14,
                name="Karthus",
                type="Mago",
                Q="SEMBRAR LA DESTRUCCIÓN",
                W="MURO DE DOLOR",
                E="PROFANACIÓN",
                R="RÉQUIEM",
                image = R.drawable.karthus
            ),
            Pokemon(
                id=15,
                name="Leesin",
                type="Coloso",
                Q="ONDA SÓNICA / GOLPE RESONANTE",
                W="SALVAGUARDA / VOLUNTAD DE HIERRO",
                E="TEMPESTAD / INCAPACITAR",
                R="IRA DEL DRAGÓN",
                image = R.drawable.leesin
            ),
            Pokemon(
                id=16,
                name="Lux",
                type="Mago",
                Q="ENLACE DE LUZ",
                W="BARRERA PRISMÁTICA",
                E="SINGULARIDAD BRILLANTE",
                R="CHISPA FINAL",
                image = R.drawable.lux
            ),
            Pokemon(
                id=17,
                name="Malaphite",
                type="Tanque",
                Q="FRAGMENTO SÍSMICO",
                W="ATRONAR",
                E="GOLPE EN EL SUELO",
                R="FUERZA IMPARABLE",
                image = R.drawable.malaphite
            ),
            Pokemon(
                id=18,
                name="Maestro Yi",
                type="Asesino",
                Q="GOLPE FULGURANTE",
                W="MEDITAR",
                E="ESTILO WUJU",
                R="IMPARABLE",
                image = R.drawable.masteryi
            ),
            Pokemon(
                id=19,
                name="Miss Fortune",
                type="Tirador",
                Q="REDOBLE",
                W="ALARDE",
                E="QUE LLUEVA",
                R="LLUVIA DE BALAS",
                image = R.drawable.missfortune
            ),
            Pokemon(
                id=20,
                name="MordeKaiser",
                type="Coloso",
                Q="ANIQUILACIÓN",
                W="INDESTRUCTIBLE",
                E="ABRAZO DE LA MUERTE",
                R="REINO DE LA MUERTE",
                image = R.drawable.mordekaiser
            ),
        )
    }
}
