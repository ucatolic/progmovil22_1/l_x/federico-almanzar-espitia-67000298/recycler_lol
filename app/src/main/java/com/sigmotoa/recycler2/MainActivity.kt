package com.sigmotoa.recycler2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.sigmotoa.recycler2.adapter.PokemonAdapter

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initRecycler()
    }

    fun initRecycler()
    {
        val recycler:RecyclerView=findViewById(R.id.rv_pokemons)
        recycler.layoutManager=LinearLayoutManager(this)

        //recycler.layoutManager=GridLayoutManager(this, 7)
        recycler.adapter=PokemonAdapter(Pokemons.pokemonList)
    }
}